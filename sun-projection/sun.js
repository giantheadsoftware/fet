import { path, select, curveCatmullRomClosed, curveCatmullRom} from "https://cdn.jsdelivr.net/npm/d3@7/+esm";
import { line } from "https://cdn.jsdelivr.net/npm/d3-shape@3/+esm";

const baseVernalEquinoxUtc = new Date(Date.UTC(2024, 2, 20, 3, 6)); // 20 March 03:06 UTC
const baseVernalEquinoxLng = 134.5;  // this is about where the sun was at the moment of the base equinox
const solarDayAvg = 86400.002;
const solarYearDaysAvg = 365.24219;
const solarYearSec = solarYearDaysAvg * solarDayAvg;
const tropicalAmplitude = 23.43;

var interval;

export const subsolar = {
  lat: 4,
  long: -167.38
}

const refraction = {
  sunset: {a: 0, color:"darkorange", eastPoints: [], westPoints: []},
  twilight1: {a: 6, color: "red", eastPoints: [], westPoints: []},
  twilight2: {a: 12, color: "blue", eastPoints: [], westPoints: []},
  night: {a: 18, color: "black", eastPoints: [], westPoints: []}
}

export const flat = {
  rSunlight: 6240 // sun visible @6240nm from subsolar point
}

function subSolarAtDate(date) {
  const equinoxDeltaSec = (date.getTime() - baseVernalEquinoxUtc.getTime()) / 1000;
  const fractionOfSolarYear = (equinoxDeltaSec % solarYearSec) / solarYearSec;
  const subSolarLat = tropicalAmplitude * Math.sin(fractionOfSolarYear*2*Math.PI);
  const fractionSolarDay = (equinoxDeltaSec % solarDayAvg)/solarDayAvg;
  const subSolarLong = (360 - fractionSolarDay*360) + baseVernalEquinoxLng; // longitude decreases as time increases

  console.log(`Subsolar @ ${date}: ${Math.trunc(subSolarLat)}° ${subSolarLat%1*60}' x ${Math.trunc(subSolarLong)}° ${subSolarLong%1*60}'`);
  return [subSolarLat, subSolarLong];
}

export function gotoTime(date, svg, graph) {
  const subsolarPos = subSolarAtDate(date);
  const latTxt = document.getElementById("subsolar-lat").value = Math.trunc(subsolarPos[0]) + ' ' + (Math.abs(subsolarPos[0])%1*60).toFixed(0);
  const longTxt = document.getElementById("subsolar-long").value = Math.trunc(subsolarPos[1]) + ' ' + (Math.abs(subsolarPos[1])%1*60).toFixed(0);
  subsolarChanged(svg, graph);
}

export function run(on, svg, graph) {
  if (!interval && on) {
    console.info("starting clock");
    gotoTime(new Date(), svg, graph);  // jump to current date before starting the interval so we don't have to wait
    interval = setInterval(() => {
      gotoTime(new Date(), svg, graph);
    }, 4000);
  } else if (!on && interval) {
    console.info("stopping clock");
    clearInterval(interval);
    interval = undefined;
  }
}

export function subsolarChanged(svg, graph) {
  // console.info("subsolarChanged");
  const latTxt = document.getElementById("subsolar-lat").value;
  const longTxt = document.getElementById("subsolar-long").value;
  const latSplit = latTxt.split(' ');
  const longSplit = longTxt.split(' ');
  const latDeg = Number(latSplit[0]);
  const longDeg = Number(longSplit[0]);
  const newLat = latSplit.length == 1 ? latDeg : latDeg + Number(latSplit[1])/60.0*(latDeg < 0 ? -1 : 1);
  const newLong = longSplit.length == 1 ? longDeg : longDeg + Number(longSplit[1])/60.0*(longDeg < 0 ? -1 : 1);
  // if (!rendered) {
  //   drawGraph();
  //   rendered = true;
  // }
  if (subsolar.lat != newLat || subsolar.long != newLong) {
    subsolar.lat = newLat;
    subsolar.long = newLong;
    drawSunLine(svg, graph);
  }
}

export function drawSunLine(svg, graph) {
  // console.log('drawing sunline');
  let sunG = svg.select("#sun");
  if (sunG.empty()) {
    sunG = svg.append("g").attr("id", "sun");
  } else {
    sunG.selectAll("*").remove();
  }
  const sunLineG = sunG.append("g").attr("id", "sunline");
  const sunLatRads = subsolar.lat*Math.PI/180;
  const sunLongRads = subsolar.long*Math.PI/180;

  // const angleInc = subsolar.lat >= 1 ? .1 : subsolar.lat/10;

  Object.entries(refraction).forEach(([name, refParams]) => {
    const angleInc = subsolar.lat - refParams.a == 0 ? .5 : Math.abs((subsolar.lat - refParams.a)/20);
    let angle = -90;
    angle += angleInc;
    let eastPoints = [];
    let westPoints = [];
    while(angle < 90) {  // never process ±90 exactly -- it blows up the atan() function
      let angleRad = angle * Math.PI/180;
      let refractRad = refParams.a *Math.PI/180;
      // let lambdax = Math.asin(Math.sin(angleRad)*Math.cos(sunLatRads));
      // let phi = Math.atan(Math.tan(angleRad)*Math.sin(sunLatRads));
      // let phix = phi + sunLongRads + Math.PI/2;
      // sin-1(cosαsinλcosλv – sinαsinλv)
      let lambdax = Math.asin(Math.cos(refractRad)*Math.sin(angleRad)*Math.cos(sunLatRads) - Math.sin(refractRad)*Math.sin(sunLatRads));
      
      // let phi = Math.atan(Math.tan(angleRad)*Math.sin(sunLatRads));
      // let phiDelta = Math.atan(Math.tan(refractRad) / Math.cos(angleRad));
      // let phix = phi + sunLongRads + Math.PI/2 + phiDelta;
      // console.log(`@ angle ${angle} lambdax = ${lambdax*180/Math.PI}; phix = ${phix*180/Math.PI}`);
      //tan-1(tanλsinλv + tanαcosλv/cosλ)
      let phix = Math.atan(Math.tan(angleRad)*Math.sin(sunLatRads) + Math.tan(refractRad)*Math.cos(sunLatRads)/Math.cos(angleRad));
      phix += sunLongRads + Math.PI/2;
      eastPoints.push ({
        a: angle,
        x: graph.center + Math.cos(phix)*((Math.PI/2-lambdax)*graph.radius/Math.PI),
        y: graph.center - Math.sin(phix)*((Math.PI/2-lambdax)*graph.radius/Math.PI)
      });

      angleRad += Math.PI;
      // lambdax = Math.asin(Math.sin(angleRad)*Math.cos(sunLatRads));
      // phi = Math.atan(Math.tan(angleRad)*Math.sin(sunLatRads));
      // phix = phi + sunLongRads - Math.PI/2;
      // lambdax = Math.asin(Math.sin(angleRad)*Math.cos(sunLatRads)*Math.cos(refractRad));
      lambdax = Math.asin(Math.cos(refractRad)*Math.sin(angleRad)*Math.cos(sunLatRads) - Math.sin(refractRad)*Math.sin(sunLatRads));
      // phi = Math.atan(Math.tan(angleRad)*Math.sin(sunLatRads));
      // phiDelta = Math.atan(Math.tan(refractRad)/Math.cos(angleRad));
      // phix = phi + sunLongRads - Math.PI/2 + phiDelta;
      phix = Math.atan(Math.tan(angleRad)*Math.sin(sunLatRads) + Math.tan(refractRad)*Math.cos(sunLatRads)/Math.cos(angleRad));
      phix += sunLongRads - Math.PI/2;
      westPoints.push ({
        a: angle + 180,
        x: graph.center + Math.cos(phix)*((Math.PI/2-lambdax)*graph.radius/Math.PI),
        y: graph.center - Math.sin(phix)*((Math.PI/2-lambdax)*graph.radius/Math.PI)
      });
      angle += angleInc;
    }
    let points = eastPoints.concat(westPoints);

    // sunLine.attr("style", `font-size: 8pt`);
    const l = line(points)
      .x(d => d.x)
      .y(d => d.y);
    const frontSide = subsolar.lat - refParams.a > 0;
    sunLineG.append("path").attr("id", name)
        .datum(points)
        .attr("fill", frontSide > 0 ? "yellow" : "black")
        .attr("fill-opacity", name === 'sunset' ? ".15" : frontSide > 0 ? "0" : .1)
        .attr("stroke", refParams.color)
        .attr("stroke-width", 1.5)
        .attr("d", line().x(d => d.x).y(d => d.y).curve(curveCatmullRomClosed.alpha(0.5)));
    sunLineG.append("circle")
        .attr("cx", points[0].x)
        .attr("cy", points[0].y)
        .attr("r", .5)
        .attr("stroke", "red")
        .attr("stroke-width", ".5");
  });

  // draw sun a subsolar point
  const subsolarG = sunG.append("g").attr("id", "subsolar");
  const cx = graph.center + Math.cos(sunLongRads)*((Math.PI/2-sunLatRads)*graph.radius/Math.PI);
  const cy = graph.center - Math.sin(sunLongRads)*((Math.PI/2-sunLatRads)*graph.radius/Math.PI);
  subsolarG.append("circle")
      .attr("cx", cx)
      .attr("cy", cy)
      .attr("r", 10)
      .attr("fill", "yellow")
      .attr("fill-opacity", ".4")
      .attr("stroke", "red")
      .attr("stroke-width", ".5");
  subsolarG.append("circle")
      .attr("cx", cx)
      .attr("cy", cy)
      .attr("r", .25)
      .attr("stroke", "red")
      .attr("stroke-width", ".5");
      
  /// flat earth sun range
  subsolarG.append("circle")
      .attr("id", "flat6240nm")
      .attr("cx", cx)
      .attr("cy", cy)
      .attr("r", flat.rSunlight * graph.radius / (180*60))
      .attr("fill", "yellow")
      .attr("fill-opacity", ".15")
      .attr("stroke", "red")
      .attr("stroke-width", ".5");
  subsolarG.append("text")
      // .attr("text", "???")
      .attr("stroke", "gold")
      .attr("stroke-width", "1")
      .attr("fill", "red")
      .attr("style", `font-size: 12pt;`)
      .attr("x", cx)
      .attr("y", cy)
      .text('Sun');
}