import { create, path, select, curveCatmullRomClosed, zoom, zoomIdentity, scaleLinear, interpolateRound } from "https://cdn.jsdelivr.net/npm/d3@7/+esm";
import { csvParse } from "https://cdn.jsdelivr.net/npm/d3-dsv@3/+esm";
import { arc, line } from "https://cdn.jsdelivr.net/npm/d3-shape@3/+esm";
// import { path } from "https://cdn.jsdelivr.net/npm/d3-path@3/+esm";
import { drawSunLine, subsolarChanged, run, gotoTime } from "./sun.js";

var cityData = [];
var rendered = false;
const graph = {
  radius: 600,
  pad: 100,
  // center: pad + radius,
  // distPerLatDegree: radius/180,
  latitudeIncrement: 15,
  // latitudeRings: 180/latitudeIncrement,

  longitudeIncrement: 15,
  // longitudeSpokes: 180/longitudeIncrement,

  clutterToleranceDegrees: 2,
  bigCityPop: 0
}
function setRadius(r) {
  graph.radius = r;
  graph.center = graph.radius + graph.pad;
  graph.distPerLatDegree = r/180;
}
function setPad(pad) {
  graph.pad = pad;
  graph.center = graph.radius + graph.pad;
}

function setLatInc(inc) {
  graph.latitudeIncrement = inc;
  graph.latitudeRings = 180/inc;
}

function setLongInc(inc) {
  graph.longitudeIncrement = inc;
  graph.longitudeSpokes = 180/inc;
}

export function graphChanged() {
  // find UI elements and update
  drawAll();
}

export function subsolarTxtChanged() {
  subsolarChanged(svg, graph);
}

export function setTime(date) {
  gotoTime(date ? date : new Date(), svg, graph);
}

export function runClock(on) {
  run(on, svg, graph);
}

setRadius(600);
setPad(100);
setLatInc(15);
setLongInc(15);

const svgDiv = document.getElementById('svg-div');
const msg0 = document.getElementById('msg0');
const svg = create('svg')
  .attr("id", "svg-graph")
  .attr("width", 2*(graph.center))
  .attr("height", 2*(graph.center));

/*------------ init code ---------*/

fetch('asset/worldcities2024.csv')
  .then(response => response.text())
  .then((data) => {
    // console.log('csv loaded');
    cityData = csvParse(data, row => {
      return {
        city: row.city,
        lat: row.lat,
        lng: row.lng,
        population: row.population
      };
    })
    console.log(`${cityData.length} cities parsed`);
    
    drawCitiesLater();
  })

// drawGraph();
svgDiv.append(svg.node());

const topG = svg.append("g").attr("width", "100%").attr("height", "100%");
const svgZoom = zoom()
    .scaleExtent([1, 64])
    .on("zoom", zoomed);
const scaleX = scaleLinear()
    .domain([0, graph.center*2])
    .range([0, graph.center*2]);
const scaleY = scaleLinear()
    .domain([0, graph.center*2])
    .range([0, graph.center*2]);
svg.call(svgZoom).call(svgZoom.transform, zoomIdentity);
drawGraph();

/*-------- functions -------------*/

function drawGraph() {
  // select("#svg-graph").selectAll("*").remove();
  // drawCitiesLater();
  drawMap();
}

function drawAll() {
  // drawCitiesLater();
  drawGraph();
  drawSunLine(svg, graph);
}

function drawMap() {
  let grid = svg.append("g").attr("id", "grid");
  if (grid.empty()) {
    grid = grid.append("g").attr("id", "grid");
  } else {
    grid.selectAll("*").remove();
  }
  const labels = grid.append("g").attr("id", "labels");
  const rings = grid.append("g").attr("id", "rings");

  for (let i = 1; i <= graph.latitudeRings; i++) {
    const r = i * graph.distPerLatDegree * graph.latitudeIncrement;
    const equator = i == graph.latitudeRings/2;
    rings.append("path")
      .attr("transform", `translate(${graph.center},${graph.center})`)
      .attr("stroke", equator ? "red" : "black")
      .attr("stroke-width", equator ? "1" : ".5")
      .attr("d", arc()({
        innerRadius: r, //equator ? r - .25 : r - .1,
        outerRadius: r, //equator ? r + .25 : r + .1,
        startAngle: 0,
        endAngle: 2 * Math.PI
      }));
  }

  [23.43, 66.57].forEach((lat, i) => {
    let r = graph.distPerLatDegree * (90-lat);
    rings.append("path")
      .attr("transform", `translate(${graph.center},${graph.center})`)
      .attr("stroke", i == 0 ? "orange" : "lightblue")
      .attr("stroke-dasharray", "3 4")
      .attr("stroke-width","1.5")
      .attr("d", arc()({
        innerRadius: r, //equator ? r - .25 : r - .1,
        outerRadius: r, //equator ? r + .25 : r + .1,
        startAngle: 0,
        endAngle: 2 * Math.PI
      }));
    r = graph.distPerLatDegree * (90+lat);
    rings.append("path")
      .attr("transform", `translate(${graph.center},${graph.center})`)
      .attr("stroke", i == 0 ? "orange" : "lightblue")
      .attr("stroke-dasharray", "3 4")
      .attr("stroke-width","1.5")
      .attr("d", arc()({
        innerRadius: r, //equator ? r - .25 : r - .1,
        outerRadius: r, //equator ? r + .25 : r + .1,
        startAngle: 0,
        endAngle: 2 * Math.PI
      }));
  });

  const spokes = grid.append("g").attr("id", "spokes");
  // const spokes = svg.append("g");
  const p = path();
  p.moveTo(-graph.radius, 0);
  p.lineTo(graph.radius, 0);

  for (let i = 0; i < graph.longitudeSpokes; i++) {
    const angle = i * graph.latitudeIncrement/* * Math.PI / 180*/;
    const quarter = angle % 90 == 0;

    console.info(p.toString());
    spokes.append("path")
      // translate to the center of the rings, and rotate by the current increment
      .attr("transform", `translate(${graph.center},${graph.center}),rotate(${angle})`)
      .attr("fill", "none")
      .attr("stroke", quarter ? "red" : "black")
      .attr("stroke-width", quarter ? "1" : ".5")
      .attr("d", p.toString())

    // label the positive ends of the spokes
    const rads = angle*Math.PI/180;
    let x = graph.center + Math.cos(rads)*(graph.radius+graph.pad/4) - Math.sin(rads/4)*35;
    let y = graph.center - Math.sin(rads)*(graph.radius+graph.pad/4) + 5;
    labels.append("text")
      .attr("class", "lng-label")
      .attr("x", x)
      .attr("y", y)
      .text(`${angle}°`)

    // label the negative ends of the spokes
    x = graph.center - Math.cos(rads)*(graph.radius+graph.pad/4+20) - Math.sin(rads/4)*35;
    y = graph.center + Math.sin(rads)*(graph.radius+graph.pad/4) + 5;
    labels.append("text")
      .attr("class", "lng-label")
      .attr("x", x)
      .attr("y", y)
      .text(`${angle == 0 ? "±180" : angle-180}°`)
  }
}

function drawCitiesLater() {
  msg0.innerHTML = `Rendering ${cityData.length} cities...`;
  setTimeout(() => {
    drawCities();
    msg0.innerHTML = "";
  }, 0);
}

function drawCities() {

  let cities = svg.select("#cities");
  if (cities.empty()) {
    cities = svg.append("g").attr("id", "cities");
  } else {
    cities.selectAll("*").remove();
  }
  cities.attr("style", `font-size: 2pt`)
      .attr("stroke-width", ".1")
      .attr("style", `font-size: 2pt;`);
  const namedCities = [];
  cityData.forEach(city => {
    const bigCity = city.population > graph.bigCityPop;
    const cluttered = namedCities.some(nc => Math.abs(nc.lng - city.lng) < graph.clutterToleranceDegrees && Math.abs(nc.lat - city.lat) < graph.clutterToleranceDegrees);
    if (bigCity && !cluttered) {
      namedCities.push(city);
    }
    let x = graph.center + Math.cos(city.lng*Math.PI/180)*((90-city.lat)*graph.radius/180);
    let y = graph.center - Math.sin(city.lng*Math.PI/180)*((90-city.lat)*graph.radius/180);
    cities.append("text")
      // .attr("transform", `translate(${radius+100},${radius+100}),rotate(${angle})`)
      // .attr("text", "???")
      .attr("stroke", "darkgreen")
      .attr("fill", "darkgreen")
      .attr("x", x)
      .attr("y", y)
      .text('.')
  });
  namedCities.forEach(namedCity => {
    let x = graph.center + Math.cos(namedCity.lng*Math.PI/180)*((90-namedCity.lat)*graph.radius/180);
    let y = graph.center - Math.sin(namedCity.lng*Math.PI/180)*((90-namedCity.lat)*graph.radius/180);
    cities.append("text")
      // .attr("transform", `translate(${radius+100},${radius+100}),rotate(${angle})`)
      // .attr("text", "???")
      .attr("stroke", "blue")
      .attr("fill", "white")
      .attr("x", x)
      .attr("y", y)
      .text(`. ${namedCity.city}`)
  });
}

function zoomed({transform}) {
  const zx = transform.rescaleX(scaleX).interpolate(interpolateRound);
  const zy = transform.rescaleY(scaleY).interpolate(interpolateRound);
  const groups = svg.selectAll("svg> g");
  groups.attr("transform", transform);//.attr("stroke-width", .1 / transform.k);
  // svg.call(grid, zx, zy);
}