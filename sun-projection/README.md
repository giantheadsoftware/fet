# sun-projection

## Name
Sun Projection

## Description
This is an interactive, graphical webpage **showing the difference between flat and globe predictions of day/night zones**.  

The idea is to plot the day/night predictions of both models onto a common flat earth map.  Several thousand global cities are displayed according to their lat/long locations.  **The graph will show which cities should be experiencing day/night/sunrise/sunset at any given time.**

These **conditions can be independently verified** by checking sunrise/sunset times online or in print, or by **viewing live conditions via web cams** in cities where that's available.  This verification should show which model works and which does not.

There is a range of locations where both models predict approximately the same day/night/sunrise/sunset observations.  This occurs mainly in the northern hemisphere, and during the summer solstice.  However, there are enough large differences that simple observations can confirm.

The bigegst differences are mainly in the southern hemisphere, at all times of the year.  These differences are amplified even more when the sun is in the southern hemisphere, maxxed at the winter solstice.

## Installation
This is a standalone web page that can be deployed behind any web server.  It contains mmultiple JavaScript files in addition to the inde.html, so modern browsaers may not allow it to run directly from the file system, without a web server.

For a web server such as Apache or nginx, just clone or copy the files to a directory and expose that directory at the path `/daynightmap`

## Usage
The page loads something like this:
![full image](./asset/doc/full.png)

At the top is a command panel, with the graph rendering below.

### Command Panel
![command panel](./asset/doc/command.png)

#### latitude°
Set the latitude, in degrees, of the [subsolar point](https://en.wikipedia.org/wiki/Subsolar_point), which is where the sun is directly "above" the Earth.  It accepts decimal degrees `15.23` or degree-minute notation such as `15 38`.  Use a space to separate degrees and minutes, and include no degree or minute symbols -- just the numbers.  

The usable latitude range is ±90°.

When setting a negative value, just negate the degree value, not the minutes.

> The degree-minute format can accept decimal values, not just integers.  You can get values less that 1 minutes by adding decimal places to the minute portion: `15 38.5`

#### longitude°
Set the longitude, in degrees, of the [subsolar point](https://en.wikipedia.org/wiki/Subsolar_point).  This field accepts the same formats as the latitude field.

The usable longitude range is ±180°.

When setting a negative value, just negate the degree value, not the minutes.

### run
The run checkbox enables/disables an interval timer that updates the graph every 4 seconds.  This is approximately the amount of time it takes for the sun's position to change by 1' of longitude.

When the run box is checked, the graph will track the current position of the sun.

> The sun's location is computed using very simple math.  It doesn't account for the eccentricity of the Earth's orbit, polar procession, etc.  It assumes a circular orbit with constant tilt and constant speeds of rotation and revolution.  This gets pretty close, but doesn't exactly match reality.  It seems to be within a few minutes (i.e. a few nautical miles) of the true location.   
> There are [other sites](https://en.wikipedia.org/wiki/Subsolar_point) that are more dedicated to exact calculations.  Visit those if you want to see a precise location at a specific time.

### now
Click this button to jump the graph to the current date-time.  Run mode will be disabled.

### summer solstice
Click this button to set the latitude to `23 26`.  The longitude is not changed.  Run mode will be disabled.

### winter solstice
Click this button to set the latitude to `-23 26`.  The longitude is not changed.  Run mode will be disabled.

### equinox
Click this button to set the latitude to `0`.  The longitude is not changed.  Run mode will be disabled.

### Subsolar Calculator
Convenience link to [timeanddate.com](https://www.timeanddate.com/worldclock/sunearth.html)'s subsolar calculator/display.  This can provide latitude and longitude settings for any date-time.

### Graph

The graph is an [azimuthal projection](https://en.wikipedia.org/wiki/Map_projection) as a circle with the north pole in the center and the south "pole" wrapped around the outer circumference.  This projection is commonly used for flat earth representation.  It features:

* accuracy of longitude/latitude at all locations (i.e. cities)
* accuracy of distance in the north-south direction
* overall accuracy near the north pole 
* longitudional distortion increases with distance from the north pole

#### Latitude Rings
Each concentric ring represents 15° latitude from the north pole.  This equates to 900 nautical miles (nm) per ring, or 1035mi (using 69mi/nm).

0° latitude is at the equatorial ring (red).  90° is at the north pole and -90° is the south pole/limit.

Some other rings are provided:
* **arctic circle**: blue dashed ring at 66.57°
* **tropic of cancer**: orange dashed ring at 23.42°
* **tropic of capricorn**: orange dashed ring at -23.43°
* **antarctic circle**: blue dashed ring at -66.57°

#### Longitude Spokes
Each radial "spoke" represents 15° of longitude.  These lines run directly north-south, creating a radial spoke pattern on this projection.

The line for 0° longitude points horizontally right.  This is the [prime meridian](https://en.wikipedia.org/wiki/IERS_Reference_Meridian).  The anti-meridian at ±180° points left in the opposite direction.  These lines are red.  The meridians at ±90° point directly up and down, also in red.

The longitude lines are labeled in positive increments going east (counterclockwise) and negative going west (clockwise).

### World Cities
City names and locations are provided by the free CSV downloaded from [simplemaps.com](simplemaps.com) (Pareto Software, LLC). Almost 48,000 cities are included in that file.

All cities are marked with a dark green dot.  This provides a vague impression of Earth's land masses.  Larger city names are displayed.  To reduce clutter, a large city will block out a square of 2°x2°, where no smaller city names will appear.  A small city will be named only if there are no larger cities nearby.

![americas image](./asset/doc/americas.png)   
_The sun over the American continents, sun "limit" circle seen in the lower left corner_

![sa-zoom image](./asset/doc/sa-zoom.png)   
_Zoomed on parts of Argentina and Uruguay_

#### Sun Marker
![sun marker image](./asset/doc/sun-marker.png)

The sun marker is a small circle labeled "Sun".  The red dot in the center is located at the latitude-longitude specified in the command panel.

#### Flat Earth Sun Circle
Surrounding the sum is a larger circle filled with pale yellow.  This is the fixed distance that's often described as the viewing limit of the sun over a flat earth.

The radius of this circle was measured form emprical data, not taken from any reference.  It's difficult to find consensus of this radius in the flat earth community.  I took the following steps to produce a radius:

1. Look up the sunset time in New York City on the summer solstice.
1. Look up the subsolar location at that moment in time -- sunset in NYC on that date.
1. Measure the flat earth distance between NYC and the subsolar point.

This produced a distance of 6240nm (7176mi).  

My understanding of flat earth night is that atmospheric conditions prevent sunlight from traveling beyond a certain distance, causing night to occur.  Assuming that these conditions apply equally in all directions, this produces a circle around the sun at a distance of 6240nm.  This is displayed as the flat earth sun circle.

I also assume that the durations of flat earth twilight are intended to match observations.  However, I only display the ring where the sunrise/sunset occurs, which translates to the boundary between the civil and nautical twilights.  It's the moment when the sun is seen disappearing from view, if there's a clear view of the horizon.

#### Globe Earth Sun Zones
For the globe Earth model, four zones are rendered:  sunset, civil dusk, nautical dusk, and night.  There's a decent discussion [here](https://www.omnicalculator.com/physics/sunset).

![refraction image](./asset/doc/refraction.png)   
_Conceptual rendering of refraction effects_

As the sun refracts around the curve of the Earth, it has effects on the opposite "dark" side, beyond the precise horizon line.  The zones are calculated as follows:

* **sunset/sunrise:  0°-6°** beyond the horizon   
In this zone we see refracted sunlight, as the sun is actually beyond the horizon.  This is the beginning of the civil twilight period.
* **civil dusk: 6°-12°** beyond the horizon   
The refracted sun visibly disappears, but the sky is still lit.  Civil twilight ends, and nautical twilight begins.
* **nautical dusk: 12°-18°** beyond the horizon   
Light in the sky dims to where larger stars are visible.  Nautical twilight changes to astronomical twilight.
* **night: 18°+** beyond the horizon   
No light from the sun is visible on the horizon.  This is full night.

> Note that the sequence is reversed in the morning before sunrise.

These lines were computed by analyzing the rings on the globe and mapping them mathematically onto the flat surface.  The math was non-trivial, but was based in simple high school geometry/trigonometry principles.  Refer to the Analysis section for details.

## Analysis
Considering the following sketches, viewing the globe Earth form 3 different perspectives:

![west view](./asset/doc/west-view.png)
_Viewing the globe from the west (z-axis), with sunlight entering from the right at the equator_

![north view](./asset/doc/north-view.png)
_Viewing the globe from the north pole (y-axis), with sunlight entering from the right at the equator_

![x view](./asset/doc/x-view.png)
_Viewing the globe from the sun's location (y-axis), 0° longitude at the equator_

![work](./asset/doc/work.png)   
_This is the sketch used to visualize and analyze the math_

## Roadmap
None planned

## Contributing
Anyone can fork and make a merge request, but the project isn't closely monitored, and isn't an active effort.

## Authors and acknowledgment
Chris Lininger

## License
Open source, Apache-2.0

## Project status
Complete until further notice
